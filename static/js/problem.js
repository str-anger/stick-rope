function hideSpinner() {
	$('#spinner').hide();
}

function showSpinner(txt) {
	$('#spinner #btntxt').text(txt);
	$('#spinner').show();
}

function showUploadControls() {
	$('#uploadcontrols').show({ duration: 700 });
}

function dzInit() {
	$('#mydz')
		.html('<div class="dz-message" data-dz-message>'+
			'<span>Drop your code here<br/>'+
			'or click to show dialog</span></div>');
	Dropzone.options.mydz = {
		success : function() {
			hideSpinner();
			showUploadControls();
		},
		drop: function() {
			showSpinner('Uploading...');
		}
	};
}

function resultToOutput(data) {
	var x = '<table class="table table-bordered sortable" style="display:none">';
	x += '<tr><th>Test</th><th>Test ID</th>'+
		'<th>Code</th><th>Message</th></tr>';
	for (k in data) {
		var v = data[k];
		x += "<tr><td>" + k + "</td><td>" + v.id +
		"</td><td>" + v.code + "</td><td><pre style='font-size:x-small'>" + v.reason +
		"</pre></td></tr>";
	}
	x += "</table>";
	return x;
}

function runCompile(url) {
	showSpinner('Compiling and running tests...');
	$.post(url,
		{ lang : $('#lang').val() },
		function (data) {
			$('#sendcompile').hide();
			$('.result').html(resultToOutput(data));
			$('.result table').show({ duration: 1000 });
		}
	);
}
