var path = require('path');
var fs = require('fs-extra');
var runner = require('./run.js');

function save(text, file) {
    var dest = path.join(global.config.default_sandbox, file);
    console.log(dest);
    fs.writeFileSync(dest, text);
    return "ok";
}

function run(bin, params, timeout, callback) {
    runner.execute(bin, params, global.config.default_sandbox, timeout, (proc) => {
        callback(proc);
    });
}

module.exports = {
    save,
    run
}