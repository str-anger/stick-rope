var fs = require('fs-extra');
var path = require('path');
var os = require('os');
var run = require('./run.js');

global.config = global.config || require('../config.json');

/**
 * 
 * @param {string} lang programming language
 * @param {string} source folder with source files
 * @param {string} destination folder with report
 * @param {function} callback function to trigger on success
 */
function run_test(lang, source, destination, callback) {
    var jp_lang = {
        "java": "java19",
        "cpp": "c/c++",
        "cpp11": "c/c++",
        "cpp14": "c/c++",
        "cpp17": "c/c++",
        "cs": "c#-1.2",
        "python": "python3",
        "python3": "python3"
    };
    var l_shell = "text";
    if (lang in jp_lang) {
        l_shell = jp_lang[lang];
    }
    var tmpDir = fs.mkdtempSync(path.join(os.tmpdir(), 'jplag-'));
    fs.chmodSync(tmpDir, '777');
    var tmpSubdir = tmpDir + "/report";

    var params = [                   // params
        '-jar',
        global.config.jplag_jar,
        '-s',
        '-l', l_shell,
        source,
        '-r', tmpSubdir
    ];
    run.execute(
        global.config.java, // java shell
        params,             // params
        '.',                // workdir 
        500000,             // timeout
        (pd) => {
            if (pd.code == 0) {
                fs.moveSync(tmpSubdir, destination, {overwrite : true});
            } 
            callback(); 
        }                   // callback    
    );

}

module.exports = { 
    run_test: run_test 
}