var domain =	require('./domain.js'),
	Contest = domain.Contest,
	Problem = domain.Problem,
	Attempt = domain.Attempt;
const path =	require('path');
const fs =		require('fs-extra');
var run =		require('./run.js');
var helpers =	require('./helpers.js'),
	monade =	helpers.monade;
var compilers = require('../lang/compilers.js');

class Execution {

	// copy code to test folder
	constructor(user, contest, problem, lang) {
		this._lang = lang;
		//todo rearrange
		var attempt = contest.getLastUserAttempt(problem.id, user.login) + 1;
		this.code_folder = path.join(
			global.config.default_code_path,
			contest.id, user.login, problem.id);

		this.test_folder = path.join(
				global.config.default_testrun_path,
				contest.id, user.login, problem.id, '' + attempt);

		this.test_data_folder = problem.location;
		try {
			fs.mkdirsSync(this.test_folder);
			helpers.chmod777(this.test_folder);
		} catch(e) { if (e.code != 'EEXIST') throw e; }
		this._problem = problem;
        this._contest = contest;
		this._user = user;
		this.fill_attempt_with_code();
	}

	fill_attempt_with_code() {
		fs.copySync(this.code_folder, this.test_folder);
		helpers.chmod777(this.test_folder);
	}

	resolve_code(code) {
		var code_descr = null;
		switch (code) {
			case -4058: code_descr = "EXEC_NOT_FOUND"; break;
			case   143: code_descr = "PROG_DIDNT_FINISH"; break;
		}
		if (code_descr) return code_descr + "(" + code + ")";
		else return code;
	}

	// compile executables and forward exec string
	compile(shell, params, callback) {
		if (typeof shell == 'function') {
			callback = shell;
			shell = null;
		}
		var handler = (ps) => {
			if (ps.code == 0) {
				if (callback) callback(ps);
			} else {
				helpers.err("CE: " + this.resolve_code(ps.code));
				helpers.err(JSON.stringify(ps));
				var NL = global.config.NEW_LINE;
				let msg = 'compilation error: ' + (ps.shell || "no_shell") + ' with code ' + ps.code;
                if (!this._contest.isStrict()) {
                    msg += NL + 'STDOUT:' + NL + (ps.stdout || '')
                         + NL + 'STDERR:' + NL + (ps.stderr || '')
                         + NL + '----------' + NL;
                }
				ps.message = msg;
				if (callback) callback(ps);
			}
		};
		try {
			let folder = path.resolve(this.test_folder);
			if (shell) {
				run.execute(shell, params, folder,
							global.config.compile.timeout * 1000,
							handler);
			} else {
				var compiler = compilers.get_compiler(this._lang);
				if (compiler == null) {
					throw {
						message: 'Not implemented default compilation method for: ' + lang,
						method: 'Execution.compile'
					};
				} else {
					compiler.compile(folder, handler);
				}
			}
		} catch (e) {
			helpers.log(e.message);
		}
	}

	copy_test_environment(test, resultObj, extra) {
		/** establishing test envorinment */
		try {
			if (fs.existsSync(test.filein_name)) {
				try {
					helpers.dbg(
						'Copying input file ' +
						test.filein_name +
						' to ' +
						test.filein_copy);
					fs.copySync(test.filein_name, test.filein_copy);
				} catch (e) {
					helpers.dbg('This happens when input file is not specified');
					helpers.warn(e.message);
				}
			}
			// copy extra files that can be required by the task
			for (var k in test.extra) {
				helpers.dbg('Copying testing env from ' + path.join(this._problem.location, test.extra[k]) + ' to ' + path.join(this.test_folder, test.extra[k]));
				fs.copySync(
					path.resolve(path.join(this._problem.location, test.extra[k])),
					path.resolve(path.join(this.test_folder, test.extra[k])));
			}
			return true;
		} catch (e) {
			// cannot provide test file
			helpers.err("[Execution:copy_test_environment] " + e.message);
			// removing sensitive info
			try {
				var _obj = {
					id: test.id,
					reason: e.message,
					code: 'TESTENV'
				};
				resultObj[test.id] = helpers.strip([_obj], this._problem.location)[0];
			} catch (e2) {
				console.log("Things happened");
				console.log(e2);
				//TODO:
			}
			return false;
		}
	}

	copy_code_from_attempt(attempt, callback) {
		// create folder if none
		try {
			fs.mkdirsSync(this.code_folder);
			helpers.chmod777(this.code_folder);
		} catch(e) { if (e.code != 'EEXIST') throw e; }
		run.clean(this.code_folder, () => {
			fs.copySync(attempt.location, this.code_folder);
			helpers.chmod777(this.code_folder);
			var rj = path.join(this.code_folder, 'run.json');
			if (fs.existsSync(rj)) fs.unlinkSync(rj);
			this.fill_attempt_with_code();
			if (callback) callback();
		});
	}

	check_result_with_test(ps, test, resultObj) {
		if (ps.code == 0) {
			var tm = this._problem.getTestObject(test.fileout_name, test.fileout_expected, this._contest.isStrict());
			var testResultObj = tm.test();
			resultObj[test.id] = testResultObj;
			resultObj[test.id].id = test.id;
		} else {
			var NL = global.config.NEW_LINE;
			resultObj[test.id] = { code: "TRUN", id: test.id };
			resultObj[test.id].reason = "Test run failed with code: " + this.resolve_code(ps.code);
            if (!this._contest.isStrict()) {
                resultObj[test.id].reason +=
                    NL + 'STDOUT: ' + helpers.cropText(ps.stdout) +
                    NL + 'STDERR: ' + helpers.cropText(ps.stderr);
            }
			helpers.log(ps);
		}
		resultObj[test.id]._lang = this._lang;
		resultObj[test.id]._start = ps._start;
		resultObj[test.id]._end = ps._end;
	}

	execute_test(compile_result, test, resultObj, cb) {
			if (!this.copy_test_environment(test, resultObj)) {
				// moving forward
				try {
					if (cb) cb();
				} catch(e) { helpers.err("execute_test fail: " + e.message); }
				return;
			}
			run.execute(compile_result.shell,
						compile_result.params,
						this.test_folder,
						this._problem.limits.time_sec * 1000,
						(ps) => {
							helpers.dbg("Launching tests");
							this.check_result_with_test(ps, test, resultObj);
							helpers.dbg("Tests finished");
							if (cb) cb();
						 },
						compile_result.options);
	}

	run_tests(compile_result, callback) {
		// prepare data for tests
		var inps = this._problem.inputs();
		var outs = this._problem.outputs();
		var extra = this._problem.extra_in || [];
		var innames = this._problem.test_in;
		var outnames = this._problem.test_out;
		var q = [];
		for (var i = 0; i < inps.length; i++) {
			q[i] = { id: i,
				filein_name: inps[i],
				fileout_name: outs[i],
				extra,
				fileout_expected: path.resolve(path.join(this.test_folder, this._problem.out)),
				filein_copy: path.resolve(path.join(this.test_folder, this._problem.in)),
			};
		}
		monade(q, (test, resultObj, cb) => {
				this.execute_test(compile_result, test, resultObj, cb)
			}, [], callback);
	}

	compile_and_run_tests(callback) {
		this.compile((p) => {
			var resultFile = path.join(this.test_folder, 'run.json');
			if (p.code != 0) {
				p.reason = p.message;
				p.code = 'CE:' + this.resolve_code(p.code);
				p.id = 'compile';
				var pe = helpers.strip([p], this.test_folder);
				var result = JSON.stringify(pe, null, '\t')
				fs.writeFileSync(resultFile, result);
				if (callback) callback(pe);
			} else {
				this.run_tests(p.result, (pe) => {
					pe = helpers.strip(pe, this.test_folder);
					var result = JSON.stringify(pe, null, '\t')
					fs.writeFileSync(resultFile, result);
					if (callback) callback(pe);
				});
			}
		});
	}

	static rerunLastAttemptsForProblem(contest, problem, users, resp) {
		if (typeof contest === 'string') contest = new Contest(contest);
		if (typeof problem === 'string') contest = new Problem(problem);
		for (var u in users) {
			var attempt =
				contest.getLastUserAttemptInstance(problem.id, users[u].login);
			if (attempt) {
				var lang = "";
				if (attempt.result && attempt.result.length > 0) {
					lang = attempt.result[0]._lang;
				} else {
					var r = {
						user: users[u].login,
						result: "Did not compile"
					};
					helpers.warn(JSON.stringify(r));
					continue;
				}
				var exec = new Execution(users[u], contest, problem, lang);
				exec.copy_code_from_attempt(attempt, () => {
					exec.compile_and_run_tests((res) => {
						var r = { user: users[u].login, result: res };
						helpers.warn(JSON.stringify(r));
					});
				});
			} else {
				var r = {user: users[u].login, result: "Not attempted"};
				helpers.warn(JSON.stringify(r));
			}
		}
	}

}

module.exports = {
	Execution: Execution
}
