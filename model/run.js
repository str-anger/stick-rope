const os = require('os');
const path = require('path');
const fs = require('fs');
const proc = require('child_process');
const spawn = proc.spawn;
const h = require('./helpers.js');

// global var holding process information
var proc_data = [];

/** execute process in windows using any sandboxing technique */
function run_win_sandboxed(shell, params, options) {
	// cwd - current working directory
	// timeout - threshold (ms) after which we do not wait and kill the process
	options = options || { cwd : '.', timeout: 10000};
	h.dbg("Compiling WIN. Options: " + JSON.stringify(options) + " | CMD: " + shell + ' ' + params);
	var my_process = spawn(shell, params, options);
	my_process._start = new Date().getTime();
	if (options.timeout) {
		setTimeout(() => {
			if (my_process.exitCode == null) {
				h.dbg('Timeout elapsed for ' + my_process.pid);
				my_process.kill();
			}
		}, options.timeout);
	}
	return my_process;
}

/** execute process in linux using any sandboxing technique */
function run_nix_sandboxed(shell, params, options) {
	// cwd - current working directory
	// timeout - threshold (ms) after which we do not wait and kill the process
	options = options || { cwd : '.', timeout: 10000 };
	var executor = global.config.executor;
	if (!options.uid && executor) options.uid = executor;
	h.dbg("Compiling *NIX. Options: " + JSON.stringify(options) + " | CMD: " + shell + " " + params);
	var my_process = spawn(shell, params, options);
	my_process._start = new Date().getTime();
	if (options.timeout) {
		setTimeout(() => {
			if (my_process.exitCode == null) {
				h.dbg('Timeout elapsed for ' + my_process.pid);

				//TODO:
				// On Linux, child processes of child processes will 
				// not be terminated when attempting to kill their parent. 
				// This is likely to happen when running a new process in a shell 
				// or with the use of the shell option of ChildProcess
				// from here: https://nodejs.org/api/child_process.html#child_process_subprocess_kill_signal

				try {
					// KILL HIM!!!
					if (shell.endsWith('a.exe')) {
						spawn("kill", ["-9", my_process.pid]);
						my_process.kill('SIGKILL'); // SIGKILL
						my_process.kill(); // SIGTERM
					} else {
						my_process.kill(); // SIGTERM
						my_process.kill('SIGKILL'); // SIGKILL
						my_process.kill('SIGHUP'); // SIGHUP
					}
				} catch (e) {
					h.dbg(e);
				}
			}
		}, options.timeout);
	}
	return my_process;
}

/** run some command using native shell
 * @param {string} shell any command that can be run in os
 * @param {array} params array of parameters
 * @param {string} workingdir place to execute
 * @callback process
 */
function execute(shell, params, workingdir, timeout, callback, options) {
	var proc = undefined;
	options = Object.assign({}, { cwd : workingdir, timeout: timeout}, options);
	switch (os.type()) {
		case 'Darwin':
		case 'Linux':
			proc = run_nix_sandboxed(shell, params, options);
			break;
		case 'Windows_NT':
			proc = run_win_sandboxed(shell, params, options);
			break;
		default:
			h.dbg('unknown os: ' + os.type());
			break;
	}
	var pid = proc.pid;
	proc_data[pid] = {
			stderr: '',
			stdout: '',
			code: -1,
			start: new Date(),
			pid: pid,
			shell: shell,
			params: params
	};
	proc.on('error',
		(err)	=> { proc_data[pid].err = err.toString(); });
	proc.stdout.on('data',
		(data)	=> { proc_data[pid].stdout += data.toString(); });
	proc.stderr.on('data',
		(data)	=> { proc_data[pid].stderr += data.toString(); });
	proc.on('close', (code) => {
		proc_data[pid]._start = proc._start;
		proc_data[pid]._end = new Date().getTime();
		proc_data[pid].code = code;
		if (callback) callback(proc_data[pid]);
	});
}

/** cleans the folder. E.g. before upload */
function clean(folder, callback) {
	try {
		var files = fs.readdirSync(folder);
		for (var i in files)
			fs.unlinkSync(path.join(folder, files[i]));
	} catch (e) {
		h.err(e);
	}
	if (callback) callback();
}

module.exports = { execute, clean }
