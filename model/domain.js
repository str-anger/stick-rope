var helpers =	require('./helpers.js');
var jplag =		require('./jplag.js');
var path =		require('path');
var fs = 		require('fs-extra');
var tests =		require('./tests.js');
var compilers = require('../lang/compilers.js');
var Execution = require('./exec.js').Execution;
var crypto =	require('crypto');

/** user of the system. */
class User {

	constructor(login) {
		if (login) {
			this.login = login;
			var userfile = User.getFile(login);
			var userText = fs.readFileSync(userfile, 'utf-8');
			var x = JSON.parse( userText );
			helpers.extend(this, x);
		} else {
			this.login = null;
		}
	}

	static getFile(login) {
		return path.resolve(path.join(global.config.default_user_path, login));
	}

	static exists(login) {
		var exists = fs.existsSync(User.getFile(login));
		return exists;
	}

	persist() {
		var userfile = path.resolve(path.join(global.config.default_user_path, this.login));
		var copy = {};
		helpers.extend(copy, this);
		delete copy.login;
	  fs.writeFileSync(userfile, JSON.stringify(copy, null, '\t'));
	}

	suicide() {
		var userfile = path.resolve(path.join(global.config.default_user_path, this.login));
		fs.unlinkSync(userfile);
		this.login = null;
	}

	isRoot() {
	    return this.type == 'root';
	}

	is(user) {
		if (typeof user == "string") {
			return this.login == user;
	    } else if (user != 'undefined') {
			return this.login == user.login;
	    }
	    return false;
	}

	getUrl() {
	    return '/u/' + this.login;
	}

	getUrlDashboard() {
	    return this.getUrl() + '/dashboard';
	}

	static getUrl(user) {
	    return '/u/' + user;
	}

	static getUrlDashboard(user) {
	    return User.getUrl(user) + '/dashboard';
	}

	static removeUrl(uid) {
		return '/delete-user/' + uid;
	}

	removeUrl() {
		return '/delete-user/' + this.login;
	}

	static editUrl() {
		return '/edit-users/';
	}
}

class Group {
	constructor(id) {
		if (id) {
			this.id = id;
			try {
				this.users = fs
					.readFileSync(path.join(global.config.default_group_path, id), 'utf-8')
					.trim()
					.split(global.config.NEW_LINE)
					.map((s) => { return s.trim(); });
			} catch (e) {
				helpers.dbg(e);
				this.users = [];
			}
		}
	}

	suicide() {
		fs.unlinkSync(path.join(global.config.default_group_path, this.id));
		this.id = undefined;
		this.users = [];
	}

	persist() {
		try {
			var file = path.join(global.config.default_group_path, this.id);
			var txt = this.users.join(global.config.NEW_LINE);
			fs.writeFileSync(file, txt);
		} catch (e) {
			helpers.err("[Group:persist()] " + e.message);
		}
	}

	getUrl(user) {
	    return user.getUrl() + '/group/' + this.id;
	}

	static getUrl(userid, groupid) {
	    return User.getUrl(userid) + '/group/' + groupid;
	}

	static editUrl() {
		return '/all-groups';
	}

	removeUrl() {
		return '/delete-group/' + this.id;
	}

	static removeUrl(gid) {
		return '/delete-group/' + gid;
	}
}

class Problem {

	constructor(id, isNew) {
		this.cfg = 'config.json';

		this.id = id;
		this.location = Problem.getFolder(id);

		if (!isNew) {
			// let it fall if we cannot find config of the problem
			var x = JSON.parse( fs.readFileSync(path.join(this.location, this.cfg), 'utf-8') );
			helpers.extend(this, x);
			try { this.statement = fs.readFileSync(path.join(this.location, x.statement), 'utf-8'); }
			catch (e) {
				// log only if debugging
				helpers.dbg(e.message);
			}
		} else {
			//TODO?
		}
	}

	static getFolder(id) {
		return path.resolve(path.join(global.config.default_testdata_path, id));
	}

	inputs() { return this.test_in.map((x) => path.join(this.location, x)); }
	outputs() { return this.test_out.map((x) => path.join(this.location, x)); }

	persist() {
		try {
			fs.mkdirSync(this.location);
		} catch (e) {}

		var copy = {};
		helpers.extend(copy, this);

		// save statement
		var txt = copy.statement;
		var file = 'task.html';
		fs.writeFileSync(path.join(copy.location, file), txt, 'utf8');
		copy.statement = file;

		// save tests
		for (var k in copy.files) {
			fs.writeFileSync(path.join(copy.location, k), copy.files[k], 'utf8');
		}
		delete copy.files;
		delete copy.cfg;
		delete copy.location;
		delete copy.id;
		fs.writeFileSync(path.join(this.location, this.cfg), JSON.stringify(copy, null, '\t'), 'utf8');
	}

	suicide() {
		fs.removeSync(this.location);
		this.id = undefined;
	}

	getTestObject(result, actualResult, isStrict) {
		var tm = this.method.type.toLowerCase();
		switch(tm) {
			case 'match':
				return new tests.MatchTest(result, actualResult, isStrict); break;
			case 'lineset':
				return new tests.LineSetMatchTest(result, actualResult, isStrict); break;
			case 'lineseqlen':
				return new tests.SeqLengthMatchTest(result, actualResult, isStrict); break;
			case 'lineseqlen+':
				return new tests.SetCountMatchTest(result, actualResult, isStrict); break;
			default:
				throw { message : "not implemented test method: " + tm, method: "Problem.getTestObject" };
		}
	}

	getUrl(user, contest, id) {
	    return contest.getUrl(user) + "/problem/" + id || this.id;
	}

	static getUrl(uid, cid, pid) {
		return Contest.getUrl(uid, cid) + "/problem/" + pid;
	}

	static editUrl(uid, pid) {
		return User.getUrl(uid) + '/editproblem/' + pid;
	}

	static editAllUrl() {
		return '/all-problems';
	}

	editUrl(uid) {
		return User.getUrl(uid) + '/editproblem/' + this.id;
	}

	static removeUrl(pid) {
		return '/removeproblem/' + pid;
	}
}

class Attempt {

	static isTextfile(f) {
		var ext = [".java", ".json", ".py", ".cs", ".cpp", ".h", ".hpp", ".c", ".rb", ".kt"];
		for (var x in ext)
			if (f.endsWith(ext[x])) return true;
		return false;
	}

	constructor(contest, problem, user, attempt) {
		this.id = attempt;
		this.problem = problem;
		this.user = user;
		this.contest = contest;
		this.location = contest.getUserAttemptLocation(problem.id, user.login, attempt);
		this.comments = this.load_comments();

		try {
			var files = fs.readdirSync(this.location);
			files = files.filter(Attempt.isTextfile);
			this.files = files.map((f) => {
				var full = path.resolve(path.join(this.location, f));
				var content = fs.readFileSync(full, 'utf-8');
				if (f == 'run.json')
					try { this.result = JSON.parse(content); } catch (e) {}
				return {
					name : f,
					fullname : full,
					text : content
				};
			});
		} catch (e) {
			helpers.err("[Attempt:constructor] " + e.message);
		}
	}

	comment_file() {
		return path.join(this.location, 'comments.teacher');
	}

	save_comments(obj) {
		try {
			var text = JSON.stringify(obj, null, '\t');
			fs.writeFileSync(this.comment_file(), text, 'utf8');
			this.comments = obj;
		} catch (e) {
			helpers.err("[Attempt:save_comments(obj)] " + e.message);
		}
	}

	load_comments() {
		try {
			if (fs.existsSync(this.comment_file()))
				return JSON.parse(fs.readFileSync(this.comment_file(), 'utf8'));
			else
				return {};
		} catch (e) {
			helpers.err("Error loading comments from file " + this.comment_file() +
				". " + e.message);
			return {};
		}
	}

	getUrl(att) {
	    let x = att || this;
	    let s = Attempt.getUrl(x.user.login, x.contest.id, x.problem.id, x.id);
	    return s;
	}

	startString(test) {
		if (this.result[test])
			if (this.result[test]._start)
				return (new Date(this.result[test]._start)).toLocaleDateString() + ' ' + (new Date(this.result[test]._start)).toLocaleTimeString();
		return "--";
	}

	timeSec(test) {
		if (this.result[test])
			if (this.result[test]._start)
				return (this.result[test]._end - this.result[test]._start) / 1000.0;
		return "--";
	}


	static getUrl(uid, cid, pid, att) {
		return Problem.getUrl(uid, cid, pid) + "/attempt/" + att;
	}

	static getCommentUrl(uid, cid, pid, att) {
		var url = Attempt.getUrl(uid, cid, pid, att) + '/comment';
		return url;
	}
}

class Contest {
	constructor(id) {
		if (id) {
			this.id = id;
			this.location = path.join(global.config.default_contest_path, id);
			var x = JSON.parse( fs.readFileSync(this.location, 'utf-8'),
				(k, v) => { if (k === 'end' || k === 'start') return new Date(v); else return v; }
			);
			helpers.extend(this, x);
			this.run_location =  path.join(global.config.default_testrun_path, this.id);
		}
	}

	hasProblem(pid) {
		return (this.problems || []).indexOf(pid) > -1;
	}

	hasGroup(gid) {
		return (this.groups || []).indexOf(gid) > -1;
	}

	getLanguages() {
		var res = { };
		var langs = this.langs || ["java"]; // default
		var all_supported_languages = compilers.get_languages();
		for (var k in all_supported_languages) {
				if (langs.indexOf(k) >= 0) {
					res[k] = all_supported_languages[k];
				}
		}
		return res;
	}

	hasLanguage(lang) {
		return this.langs ? (this.langs.indexOf(lang) >= 0) : false;
	}

	hashcode() {
		return crypto.createHash("md5").update(this.id).digest("hex")
	}

	suicide() {
		fs.unlinkSync(path.join(global.config.default_contest_path, this.id));
		this.id = undefined;
	}

	persist() {
		var contestfile = path.resolve(path.join(global.config.default_contest_path, this.id));
		var copy = {};
		helpers.extend(copy, this);
		delete copy.id;
		delete copy.location;
		delete copy.run_location;
		var str = JSON.stringify(copy, null, '\t');
    	fs.writeFileSync(contestfile, str);
	}

    isStrict() {
        return this.is_strict || false;
    }
    
	getUserAttemptLocation(problem_id, user_id, attempt_id) {
		return path.join(this.run_location, user_id, problem_id, "" + attempt_id);
	}

	getAttempts(problem, user) {
		var problem_folder = path.join(this.run_location, user.login, problem.id);
		try {
			var attempts = fs
						.readdirSync(problem_folder)
						.filter(helpers.isInt)
						.map((x) => parseInt(x))
						.sort((a, b) => a-b);
			return attempts.map((x) => {
					var d = new Attempt(this, problem, user, x);
					d = helpers.ensureTextGoodEnoughForDisplay(d);
					return d;
			}).sort((a,b) => a.id - b.id);
		} catch (e) {
			helpers.dbg(e.message);
		}
	}

	getLastUserAttempt(problem_id, user_id) {
		var p = path.join(this.run_location, user_id, problem_id);
		var result = 0;
		var dirs = [];
		try {
			dirs = fs
				.readdirSync(p)
				.filter(helpers.isInt)
				.map((x) => parseInt(x))
				.sort((a, b) => a-b);
		} catch (e) {
			helpers.dbg(e.message);
		}
		if (dirs.length) result = dirs[dirs.length - 1];
		return result;
	}

	getLastUserAttemptInstance(problem_id, user_id) {
		var att = this.getLastUserAttempt(problem_id, user_id);
		if (att == 0) return null;
		return new Attempt(this, new Problem(problem_id), new User(user_id), att);
	}

	getUrl(user) {
		return "/u/" + user.login + "/contest/" + this.id;
	}

	static getUrl(uid, cid) {
	    return "/u/" + uid + "/contest/" + cid;
	}

	static allContestsUrl() {
		return "/all-contests/";
	}

	static editUrl(cid) {
		if (!cid)
			return "/edit-contest/";
		else
			return "/edit-contest/?cid=" + cid;
	}

	editUrl() {
		return "/edit-contest/?cid=" + this.id;
	}

	static removeUrl(cid) {
		return "/delete-contest/" + cid;
	}

	removeUrl() {
		return "/delete-contest/" + this.id;
	}

	/**
	 * generates relative url for jplag report location
	 * @param {string} lang programming language
	 */
	getJplagReportUrl(lang) {
		return global.config.jplag_url + "/" + this.hashcode() + "_" + lang;
	}

	static getJplagCreateReportUrl(contest_id) {
		return "/run-jplag/" + contest_id;
	}
	/**
	 * returns the POST url to run JPlag plagiarism check
	 * @returns "/run-jplag/...contest_id..."
	 */
	getJplagCreateReportUrl() {
		return "/run-jplag/" + this.id;
	}

	/**
	 * genarates folder path to store the report
	 * @param {string} lang programming language
	 */
	getJplagReportPath(lang) {
		return path.resolve(path.join(global.config.jplag_path, this.hashcode() + "_" + lang));
	}

	/**
	 * Checks that the report already created
	 * @param {string} lang programming language 
	 */
	isJplagReportCreated(lang) {
		return fs.existsSync(this.getJplagReportPath(lang));
	}

	/**
	 * Gets the collection of url to show on a screen
	 * @returns a dict like {"url1" : "get", "url2": "post", ...}
	 */
	getJplagUrls() {
		var result = {};
		var langs = this.getLanguages(); 
		for (var lang in langs) {
			if (this.isJplagReportCreated(lang)) {
				result[this.getJplagReportUrl(lang)] = langs[lang];
			}
		}
		if (!result || Object.keys(result).length === 0) {
			result[this.getJplagCreateReportUrl()] = "post";
		}
		return result;
	}

	/**
	 * Runs JPlag test on the solution is the contest folder
	 * @param {function} callback 
	 */
	runJplag(callback) {
		var langs = this.getLanguages();
		helpers.monade(Object.keys(langs), (inp, _, cb) => {
			jplag.run_test(
					inp, 
					path.resolve(this.run_location), 
					this.getJplagReportPath(inp), 
					cb
				);
		}, [], callback);
	}

	startString() {
		return this.start.toLocaleDateString() + ' ' + this.start.toLocaleTimeString();
	}

	endString() {
		return this.end.toLocaleDateString() + ' ' + this.end.toLocaleTimeString();
	}

	startDataString() {
		return helpers.formatLocalDate(this.start);
	}

	endDataString() {
		return helpers.formatLocalDate(this.end);
	}
}

module.exports = {
	User: User,
	Problem: Problem,
	Contest: Contest,
	Group: Group,
	Attempt: Attempt
}
