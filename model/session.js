// for sha256 support
var crypto = require('crypto');
// max age - 60 minutes
var SESSION_MINUTES = 60;

// builds a key using cookies key, browser version and ip address
function getKey(req, key) {
	// session key
	var cook = req.cookies;
	if (!cook && !key) return null;
	var sk = '';
	if (key) sk = key;
	else if (cook && cook.sr_sessionkey) sk = cook.sr_sessionkey;
	else return null;

	// hash from the browser agent
	var hash = crypto.createHash('sha256');
	hash.update(req.headers['user-agent']);
	var h = hash.digest('hex');

	// key-ip-browser
	//TODO: rework. Removed browser hash for stability
	var result = sk + '_' + req.ip + '_';// + h;
	return result;
}

//TODO: clean up old ones?
/** if session for the key exisits - return it */
function getSession(req) {
	var cook = req.cookies;
	if (!cook) { return null; }
	return global.sr_sessions[getKey(req)];
}

/** Creates new session */
function setSession(req, res, key, user_name) {
	res.cookie('sr_sessionkey', key, { maxAge : (60 * 1000) * SESSION_MINUTES });
	var sk = getKey(req, key);
	global.sr_sessions[sk] = {
		start : new Date(),
		ip: req.ip,
		action: { method: req.method, url: req.path },
		key: key,
		user: user_name,
		skey: sk
	};
	return global.sr_sessions[sk];
}

/** Update session and reset cookies */
function refreshSession(req, res) {
	var s = getSession(req);
	s.start = new Date();
	s.action.method = req.method;
	s.action.url = req.path;
	res.cookie('sr_sessionkey', s.key, { maxAge : (60 * 1000) * SESSION_MINUTES });
	return s;
}

/** for logout - remove session */
function killSession(req, res) {
	var sk = getKey(req);
	res.clearCookie('sr_sessionkey');
	delete global.sr_sessions[sk];
}

function getSessionStatistics() {
	var result = [];
	for (var session_key in global.sr_sessions) {
		var s = global.sr_sessions[session_key];
		// if session expired - do not show
		var until_end = (s.start - (new Date())) + SESSION_MINUTES * 60 * 1000;
		if (until_end < 0) continue;
		var session_passed_minutes =
					Math.round(((new Date()) - s.start) / 1000 / 60);
		result.push({
			login: s.user,
			time: s.start, // Date object
			passed: session_passed_minutes,
			action: s.action
		});
	}
	// sort from freshest to oldest
	result.sort((a, b) => { return b.time - a.time; });
	return result;
}

module.exports = {
	getSession, setSession, killSession, refreshSession, getSessionStatistics,
	init : () => { global.sr_sessions = global.sr_sessions || []; return this; }
}
