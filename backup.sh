_now=$(date +"%Y%m%d-%H%M%S")
_file="backup_$_now.tar.gz"
echo "Backing up to $_file..."
tar --exclude=".git/*" --exclude="node_modules/*"  --exclude="test/.bash_history" --exclude="*.tar.gz" -zcf $_file .
