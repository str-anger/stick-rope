var path = require('path');
var fs = require('fs-extra');
var dal = require('../model/dal.js').UserData;
var h = require('../model/helpers.js');
var dom = require('../model/domain.js');
var auth = require('../model/auth.js');
var Group = dom.Group;
var User = dom.User;

/** prepare initial data object fields for page render */
function dataObject(session, ext) {
	var suser = dal.getUser(session.user);
	var result = {
		session_user: suser
	};
	if (ext) h.extend(result, ext);
	return result;
};

function edit_groups(req, res, session) {
  var groups = dal.getAllGroupsSync();
  var users = dal.getAllUsersSync();
  var dao = dataObject(session, {
    groups, users, Group
  });
  res.send(global.templates.get('all-groups')(dao));
};

function edit_groups_post(req, res, session) {
  var updates = [];
  var newgroupname;
  var newgroup;

  // string to list of logins
  var s2a = (s) => {
    return (s || "").trim()
      .split(global.config.NEW_LINE)
      .map((s) => { return s.trim(); });
  };

  // from params select all groups
  for (var key in req.body) {
    var val = req.body[key];
    if (key.startsWith('group_')) {
      var gname = key.substring(6);
      updates[gname] = s2a(val);
    } else {
      if (key == 'newgroupname') newgroupname = (val || "").trim();
      else if (key == 'newgroup') newgroup = s2a(val);
    }
  }
  if (newgroupname) updates[newgroupname] = newgroup;

  var existing = dal.getAllGroupsSync();
  var existingdict = [];
  for (var i in existing)
    existingdict[existing[i].id] = existing[i];

  for (var key in updates) {
      var g;
      if (key in existingdict) {
        g = existingdict[key];
        g.users = updates[key];
      } else {
        g = new Group();
        g.id = key;
        g.users = newgroup;
      }
      g.persist();
  }
  edit_groups(req, res, session);
};

function delete_group(req, res, session) {
  var id = req.params.gid;
  try {
    var g = new Group(id);
    if (g.id) {
      g.suicide();
      res.status(200).send("ok");
    } else
      res.status(500).send("No such group");
  } catch(e) {
    res.status(500).send(JSON.stringify(e));
  }
};

function edit_users(req, res, session) {
  var users = dal.getAllUsersSync();
  var dao = dataObject(session, {
    users, User
  });
  res.send(global.templates.get('edit-users')(dao));
}

function edit_user_post(req, res, session) {
		var uid = req.body['login'];
		var user;
		var createpass = false;
		try {
			user = new User(uid);
		} catch (e) {
			createpass = true;
			user = new User();
		}
		for (var key in req.body) {
			user[key] = req.body[key];
		}

		user.persist();

		if (createpass) {
			auth.send_newpass(user, (dao, newpass) => {
				if (dao.success) {
					h.log("Email sent successfully to user " + uid);
				} else {
					h.err("[users:edit_user_post()] " + dao.error_reason);
				}
				edit_users(req, res, session);
			});
		} else
			edit_users(req, res, session);
}

function delete_user(req, res, session) {
	var uid = req.params.uid;
	try {
		var user = new User(uid);
		user.suicide();
		res.status(200).send("User " + uid + " deleted");
	} catch(e) {
		h.dbg("Error while deleting user in users:delete_user(r,r,s)");
		h.err(e);
		res.status(500).send(JSON.stringify(e));
	}
}

function add_users_batch(req, res, session) {
	var dao = dataObject(session);
	res.send(global.templates.get('add-users-batch')(dao));
}

function add_users_batch_post(req, res, session) {
	h.dbg("New users inserted by " + session.user);
	var divider = req.body.divider;// == 'tab' ? '\t' ; req.body.divider;
	var removeQ = !!req.body.removequotes;
	var remove1 = !!req.body.removefirst;
	var sendEmail = !!req.body.email;
	var tabletext = req.body.table;
	var columns = ['email', 'fullname', 'student_id', 'group', 'pass'];
	var table = tabletext.split('\n');
	var errors = [];
	var users = [];

	for (var i = remove1 ? 1 : 0; i < table.length; i++) {
		var row = table[i];
		if (row.trim()) { // if sting contains non-space characters
			var cells = row.split(divider);
			try { // wrap parsing
				var u = {};
				for (var j in columns) {
					var field = cells[j];
					if (!field) continue;
					if ((field.charAt(0) == '"' && field.charAt(field.length - 1) == '"') ||
							(field.charAt(0) == "'" && field.charAt(field.length - 1) == "'"))
					 	field = field.substring(1, field.length - 1);
					u[columns[j]] = field;
					u.type = 'student';
				}
				var pass = u.pass || auth.generatePassword(8);
				u.hash = auth.hpass(u.email, pass);
				var gf = u.group;
				// remove sensitive fields
				delete u.group;
				delete u.pass;
				if (!dal.userExists(u.email)) { // if no user with this email
					var userfile = path.join(global.config.default_user_path, u.email);
					fs.writeFileSync(userfile, JSON.stringify(u, null, '\t'));
					fs.appendFileSync(path.join(global.config.default_group_path, gf), global.config.NEW_LINE + u.email);
					var user = dal.getUser(u.email);
					users.push(user);
					if (sendEmail) auth.send_pass(user, pass);
				} else {
					errors.push(JSON.stringify({ source: row, message: "User exists"}));
				}
			} catch (e) {
				h.err(e);
				errors.push(JSON.stringify({source: row, message: e.message }));
			}
		}
	}
	var dao = dataObject(session, { users, errors });
	res.send(global.templates.get('add-users-batch-report')(dao));
}

module.exports = {
  edit_groups, edit_groups_post,
  delete_group,
  edit_users, edit_user_post,
  delete_user,
	add_users_batch, add_users_batch_post
}
