var dal = require('../model/dal.js').UserData;
var swig = require('swig');
var path = require('path');
var fs = require('fs-extra');
var dom = require('../model/domain.js'),
	Contest = dom.Contest,
	Problem = dom.Problem,
	User = dom.User,
	Attempt = dom.Attempt,
	Group = dom.Group;
var Execution = require('../model/exec.js').Execution;
var runlib = require('../model/run.js');
var h = require('../model/helpers.js');


function prblm(id) {
	 return dal.getProblem(id) || new Problem(id, true);
}

function problem(req, res, session) {
	var suser = dal.getUser(session.user);
 	var pid = req.params.problem_id;
	var problem = prblm(pid);
	var dao = {
		session_user: suser,
		problem : problem
	};
	dao.files = h.mapfolder(Problem.getFolder(pid));
	res.send(global.templates.get('editproblem')(dao));
}

function problempost(req, res, session) {

	var suser = dal.getUser(session.user);
 	var pid = req.params.problem_id;
	var _problem = prblm(pid);
	var body = req.body;

	_problem.name = body.name;
	_problem.method = { type : body['method.type'] };
	_problem.limits = { time_sec : parseInt(body['limits.time_sec']), memory_mb : parseInt(body['limits.memory_mb']) };
	_problem.in = body.in;
	_problem.out = body.out;
	_problem.statement = body.task;
	var fins = [];
	var fouts = [];
	var files = [];
	for (var i = 1; i <= 20; i++) {
		var key_i = 'testin[' + i + ']';
		var key_o = 'testout[' + i + ']';
		var vi = body[key_i];
		var vo = body[key_o];

		if (!vi && !vo) break;
		fins.push('test' + i + '.txt');
		fouts.push('test' + i + '.out');
		files[fins[fins.length-1]] = h.normalizeEndlines(vi);
		files[fouts[fouts.length-1]] = h.normalizeEndlines(vo);
	}
	_problem.test_in = fins;
	_problem.test_out = fouts;
	_problem.files = files;
	_problem.persist();
	problem(req, res, session);
}

function delete_problem(req, res, session) {
	var pid = req.params.problem_id;
	h.dbg('Deleting problem ' + pid);
	try {
		new Problem(pid).suicide();
		res.status(200).send('ok');
	} catch (e) {
		h.dbg("Error in deleting poblem edit:delete_problem(req, res, session)");
		h.err(e);
		res.status(500).send(JSON.stringify(e));
	}
}

module.exports = {
	problem, problempost, delete_problem
}
