var dal = require('../model/dal.js').UserData;
var swig = require('swig');
var path = require('path');
var auth = require('../model/auth.js');
var mailer = require('../model/email.js');
var h = require('../model/helpers.js');
const util = require('util');

function login(req, res, message) {
	var reason = req.query.reason;
	var isPost = req.method == "POST";
	var returnurl = (isPost ? req.body : req.query).returnurl || "";
	res.send(global.templates.get('login')( { error_reason: reason, returnurl } ));
}

function chpass(req, res, session) {
	var reason = req.query.reason;
	var suser = dal.getUser(session.user);
	res.send(global.templates.get('chpass')( {
		error_reason: reason,
		session_user: suser
	} ));
}

function chpasspost(req, res, session) {
	var suser = dal.getUser(session.user);
	var old = req.body.oldpass;
	var new1 = req.body.newpass1;
	var new2 = req.body.newpass2;

	if (new1 != new2) {
		res.send(global.templates.get('chpass')( {
			changedsuccessfully: false,
			error_reason: 'New passwords do not match',
			session_user: suser
		}));
	} else {
		auth.changepass(req, res, suser.login, old, new1, (code, o) => {
			if (code == 'success') {
				res.send(global.templates.get('chpass')( {
					changedsuccessfully: true,
					session_user: suser
				}));
			} else {
				res.send(global.templates.get('chpass')( {
					changedsuccessfully: false,
					session_user: suser,
					error_reason: o.message,
				}));
			}
		});
	}
}

function resetpass(req, res) {
	var reason = req.query.reason;
	res.send(global.templates.get('resetpass')( {
		error_reason: reason
	}));
}

function resetpasspost(req, res) {
	h.access(req);
	var email = req.body.email;
	var users = dal.getAllUsersSync().filter((u) => { return u.email == email; });

	if (users.length == 0) {
		var dao = {};
		dao.error_reason = "User with this email not found";
		res.send(global.templates.get('resetpass')(dao));
	} else {
		auth.send_newpass(users[0], (dao, newpass) => {
			res.send(global.templates.get('resetpass')(dao));
		});
	}
}

module.exports = {
	login, chpass,
	chpasspost,
	rpass: resetpass,
	rpasspost: resetpasspost
}
