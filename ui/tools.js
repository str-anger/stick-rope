var box = require('../model/sandbox.js');

function sandbox(req, res, session) {
    res.send(global.templates.get('sandbox')());
}

function sandbox_save(req, res, session) {
    var filename = req.body.filename;
    var data = req.body.text;
    var result = box.save(data, filename);
    res.send(result);
}

function sandbox_run(req, res, session) {
    var bin = req.body.bin;
    var params = req.body.params;
    var timeout = parseInt(req.body.timeout);
    if (timeout <= 0) timeout = null;
    box.run(bin, params.split(' '), timeout, (data) => {
        var text = JSON.stringify(data, null, '\t').replace(/\\n/g, '\t\\\n');
        res.send(text);
    });
}

module.exports = {
    sandbox, sandbox_run, sandbox_save
}