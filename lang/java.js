var run = require('../model/run.js');
var h = require('../model/helpers.js');
var fs = require('fs');
var path = require('path');
var ct = require('./compiler-tools.js');

/**
* @ param {folder} folder with sources where files will be compiled
* @ param {callback} will accept either error object or data for execution step
*/
function compile(folder, callback) {
  // we will compile these guys
  var files = "*.java";
  // with this compiler
  var compiler = 'javac';

  // search for main class
  var javamainclass;
  try {
    // class name: search in folder among java files using regex
    javamainclass = ct.search_substring(folder, '.java',
        /public\W+static\W+void\W+main/)
        .replace('.java', '');
  } catch (e) {
    // if could not find - errormessage with code "NOMAIN"
    if (callback) callback({
      code : "NO_MAIN_METHOD",
      reason: e.message,
      method: e.method,
      stdout: e.message
    });
    return;
  }
  // compilation string
  // javac -sourcepath . -g ClassName.java
  var compile_params = ['-sourcepath', '.', '-g', javamainclass + '.java'];
  // execution string
  // java -cp folder ClassName
  var object_to_exec = { shell: 'java', params: ['-cp', folder, javamainclass] };

  //compile: compiler, parameters, working dir timeout to cancel, callback
	run.execute(compiler, compile_params, folder,
			global.config.compile.timeout * 1000,
      // callback recieves process object
			(p) => {
    		// if compiled ok (code == 0), we can execute tests
    		// For execution we provide prepared shell and params
        p.result = p.code	? null : object_to_exec;
		    if (callback) callback(p);
	     });
}

module.exports = {
  compile
}
