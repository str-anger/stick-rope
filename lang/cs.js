var run = require('../model/run.js');
var h = require('../model/helpers.js');
var fs = require('fs');
var path = require('path');
var ct = require('./compiler-tools.js');

/**
* @ param {folder} folder with sources where files will be compiled
* @ param {callback} will accept either error object or data for execution step
*/
function compile(folder, callback) {
  // we will compile these guys
  var files = "*.cs";
  // with this compiler
  var compiler = 'mcs';

  // search for main file
  var csmainclass;
  try {
    // class name: search in folder among cs files using regex
    csmainclass = ct.search_substring(folder, '.cs',
        /static\W+void\W+Main/);
  } catch (e) {
    // if could not find - errormessage with code "NOMAIN"
    if (callback) callback({
      code : "NO_MAIN_METHOD",
      reason: e.message,
      method: e.method,
      stdout: e.message
    });
    return;
  }
  // compilation string
  // mcs Main.cs
  var compile_params = [csmainclass];
  // execution string
  // mono 
  var object_to_exec = { shell: 'mono', params: [csmainclass.replace('.cs', '.exe')] };

  //compile: compiler, parameters, working dir timeout to cancel, callback
	run.execute(compiler, compile_params, folder,
			global.config.compile.timeout * 1000,
      // callback recieves process object
			(p) => {
    		// if compiled ok (code == 0), we can execute tests
    		// For execution we provide prepared shell and params
        p.result = p.code ? null : object_to_exec;
		    if (callback) callback(p);
	     });
}

module.exports = {
  compile
}
